PROGRAM TDateTimeTest;
//http://www.freepascal.org/docs-html/rtl/dateutils/comparedate.html
USES dateutils, sysutils;
CONST
	N = 128;
TYPE
	T_POST = RECORD
		txt : STRING[128];
		fecha : TDateTime;
	END;
	
	T_ARRAY = ARRAY[1..N] OF T_POST;
	
FUNCTION init(VAR arr : T_ARRAY) : BYTE;
VAR
	p : T_POST;
BEGIN
	p.txt:= 'Post n1';
	p.fecha:= EncodeDateTime(2000, 2, 1, 0,0,0,0);
	arr[1]:= p;
	
	p.txt:= 'Post n2';
	p.fecha:= EncodeDateTime(2000, 1, 31, 0,0,0,0);
	arr[2]:= p;
	
	p.txt:= 'Post n3';
	p.fecha:= EncodeDateTime(2010, 12, 5, 0,0,0,0);
	arr[3]:= p;
	
	p.txt:= 'Post n4';
	p.fecha:= EncodeDateTime(2010, 12, 3, 0,0,0,0);
	arr[4]:= p;
	
	p.txt:= 'Post n5';
	p.fecha:= EncodeDateTime(2010, 12, 3, 0,0,0,0);
	arr[5]:= p;
	
	p.txt:= 'Post n6';
	p.fecha:= EncodeDateTime(2012, 7, 21, 0,0,0,0);
	arr[6]:= p;
	
	p.txt:= 'Post n7';
	p.fecha:= EncodeDateTime(2005, 4, 30, 0,0,0,0);
	arr[7]:= p;
	
	init:= 7;
END;


PROCEDURE quickSort (VAR arr : T_ARRAY; primero, ultimo : BYTE);
VAR
	i, j, central : 1..N;
	pivote, aux: T_POST;
BEGIN
	central:= (primero + ultimo) DIV 2;
	pivote:= arr[central];
	i:= primero;
	j:= ultimo;
	
	REPEAT
		WHILE (CompareDate(arr[i].fecha, pivote.fecha) < 0) DO
			i:= i + 1;
		WHILE (CompareDate(arr[j].fecha, pivote.fecha) > 0) DO
			j := j - 1;
			
		IF i <= j THEN 
		BEGIN
			aux := arr[i];
			arr[i] := arr[j];
			arr[j] := aux;
			i:= i + 1;
			j:= j - 1;
		END
	UNTIL i > j;
	
	IF primero < j THEN
		quickSort(arr, primero, j);
	
	IF i < ultimo THEN
		quicksort(arr, i, ultimo);
END;

PROCEDURE quickSort(VAR arr : T_ARRAY; lim : BYTE);
BEGIN
	quickSort(arr, 1, lim )
END;

PROCEDURE mostrarRangos(VAR arr: T_ARRAY; lim : BYTE; DateA, DateB : TDateTime);
VAR
	i : BYTE;
BEGIN
	i:= 0;
	quickSort(arr,lim);
	Writeln('--- Rango ', DateToStr(DateA),' - ', DateToStr(DateB),' ---');
	REPEAT
		inc(i);
		WITH arr[i] DO
			IF (CompareDate(fecha, DateA) >= 0) THEN
				Writeln('  #', i,' --> ', txt,' --> ', DateToStr(fecha));
	UNTIL (CompareDate(arr[i+1].fecha, DateB) > 0) OR (i >= lim);
END;


PROCEDURE mostrarArray(VAR arr : T_ARRAY; lim : BYTE);
VAR
	i : BYTE;
BEGIN
	Writeln('--- POSTS ---');
	FOR I:= 1 TO lim DO BEGIN
		WITH arr[i] DO BEGIN
			Writeln('  #', i,' --> ', txt,' --> ', DateToStr(fecha));
		END;
	END;
END;

VAR
	arr : T_ARRAY;
	lim : BYTE;
	d,d1 : TDateTime;

BEGIN
	lim:= init(arr);
	quickSort(arr,lim);
	mostrarArray(arr,lim);
	
	Writeln; Writeln;
	
	d := EncodeDateTime(2000, 2, 1, 0,0,0,0);
	d1 :=  EncodeDateTime(2010, 12, 5, 0,0,0,0);
	mostrarRangos(arr, lim, d, d1);
	Writeln; Writeln;
END.
