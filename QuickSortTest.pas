PROGRAM QuickSortTest;
USES CRT;
CONST
	TOT = 100;
	POR_PAG = 5;
	
TYPE
	T_STRING = String[128];
	T_ARRAY = ARRAY[1..TOT] OF T_STRING;
	
PROCEDURE init (var arr : T_ARRAY; var n : BYTE);
BEGIN
	arr[1] := 'Francia';
	arr[2] := 'Japon';
	arr[3] := 'Corea';
	arr[4] := 'Italia';
	arr[5] := 'Panama';
	arr[6] := 'Bolivia';
	arr[7] := 'Peru';
	arr[8] := 'Estados Unidos';
	arr[9] := 'Mexico City';
	arr[10] := 'Canada';
	arr[11] := 'Inglaterra';
	arr[12] := 'Espana';
	arr[13] := 'China';
	arr[14] := 'Malasia';
	arr[15] := 'Israel';
	arr[16] := 'Ecuador';
	arr[17] := 'Rusia';
	arr[18] := 'Iran';
	arr[19] := 'Afghanistan';
	arr[20] := 'Africa';
	arr[21] := 'Egipto';
	arr[22] := 'Suecia';
	n:=22;
END;

PROCEDURE quickSort (VAR arr : T_ARRAY; primero, ultimo : WORD);
VAR
	i, j, central : 1..TOT;
	pivote, aux: T_STRING;
BEGIN
	central:= (primero + ultimo) DIV 2;
	pivote:= arr[central];
	i:= primero;
	j:= ultimo;
	
	REPEAT
		WHILE arr[i] < pivote DO
			i:= i + 1;
		WHILE arr[j] > pivote DO
			j := j - 1;
			
		IF i <= j THEN 
		BEGIN
			aux := arr[i];
			arr[i] := arr[j];
			arr[j] := aux;
			i:= i + 1;
			j:= j - 1;
		END
	UNTIL i > j;
	
	IF primero < j THEN
		quickSort(arr, primero, j);
	
	IF i < ultimo THEN
		quicksort(arr, i, ultimo);
END;

PROCEDURE quickSort(VAR arr : T_ARRAY; lim : WORD);
BEGIN
	quickSort(arr, 1, lim )
END;

FUNCTION binariaRecursiva(VAR arr : T_ARRAY; buscado : T_STRING; primero, ultimo : BYTE) : BYTE;
VAR
	central : BYTE;
BEGIN
	central:= (primero + ultimo) DIV 2;
	IF primero > ultimo THEN
		binariaRecursiva:= 0
	ELSE IF arr[central] = buscado THEN
		binariaRecursiva:= central
	ELSE IF buscado < arr[central]  THEN
		binariaRecursiva:= binariaRecursiva(arr, buscado, primero, central - 1)
	ELSE IF buscado > arr[central] THEN 
		binariaRecursiva:= binariaRecursiva(arr, buscado, central + 1, ultimo)
END;


// #75 = <<<<<
// #77 = >>>>>
// #27 = ESC
PROCEDURE mostrarArray (VAR arr : T_ARRAY; lim : BYTE);
VAR
	i : 0..TOT;
	cantMostrada : 0..POR_PAG;
	key : CHAR;
BEGIN
	i:=0;
	cantMostrada:= 0;
	clrscr;
	REPEAT
		inc(i);
		inc(cantMostrada);
		Writeln('  #', i ,' --> ', arr[i]);
		IF (cantMostrada = POR_PAG) OR ( (cantMostrada <> POR_PAG) AND (i = lim)) THEN
		BEGIN
			Writeln('Mostrando ',cantMostrada, ' de ', lim, ' a partir del ', i - cantMostrada + 1);
			Writeln('Flechas para navegar, ESC salir');
			key := Readkey;
			IF key = #0 THEN
			BEGIN
				key := Readkey;
				IF (key = #75) THEN
					IF ( (i - (cantMostrada + POR_PAG)) > 0 ) THEN
						i:= i - (cantMostrada + POR_PAG)
					ELSE
						i:=0;
			END;
			clrscr;
			cantMostrada:= 0;
		END;
	UNTIL (key = #27) OR (i = lim);
END;

FUNCTION menuPrincipal : CHAR;
BEGIN
	Writeln('   1 - Array original');
	Writeln('   2 - Ordenar Quicksort');
	Writeln('   3 - Busqueda Binaria');
	Writeln;
	Write('Opcion: ');
	menuPrincipal:= Readkey;
END;

VAR
	arr : T_ARRAY;
	n, encontrado : BYTE;
	sel : CHAR;
	auxBuscado : T_STRING;
BEGIN
	init(arr,n);
	REPEAT
		sel:= menuPrincipal;
		CASE sel OF
			'1' :	mostrarArray(arr,n);
			'2' :	BEGIN
						quickSort(arr, n);
						mostrarArray(arr,n);
					END;
			'3' :	BEGIN
						clrscr;
						Write('Buscar: ');
						Readln(auxBuscado);
						quickSort(arr,n);
						encontrado:= binariaRecursiva(arr, auxBuscado, 1, n);
						IF encontrado <> 0 THEN
							Writeln('  Encontrado: #', encontrado, ' ---> ', arr[encontrado])
						ELSE
							Writeln('  No encontrado');
						Readkey;
					END;
		END;
		clrscr;
	UNTIL sel = '0';
END.
	
